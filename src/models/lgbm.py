import lightgbm as lgbm
import numpy as np
import pandas as pd
from sklearn.externals import joblib
import sys, os
import glob
from datetime import datetime

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
logging.basicConfig(format='%(asctime)s | %(levelname)s : %(message)s',
                     level=logging.INFO, stream=sys.stdout)

os.chdir(os.path.join(os.path.dirname(__file__), "..", ".."))

def LightGBM_model(train_df, test_df, categorical_feats):
    """The sources of the model:
        https://www.kaggle.com/shep312/lightgbm-with-weighted-averages-dropout-783/code
        https://www.kaggle.com/mlisovyi/lightgbm-hyperparameter-optimisation-lb-0-746

        :param train_df: Training data
        :type train_df: Pandas DataFrame
        :param test_df: Test data
        :type test_df: Pandas DataFrame
        :param categorical_feats: List of categorical features
        :type categorical_feats: list

        :return: Predictions and the best AUC score
        :rtype: Numpy array and a float
    """
    var_target = 'TARGET'
    train_columns = train_df.columns.drop([var_target]).tolist()

    lgbm_train = lgbm.Dataset(data=train_df[train_columns],
                              label=train_df[var_target],
                              categorical_feature=categorical_feats,
                              free_raw_data=False)
    lgbm_params = {
        'boosting': 'dart',
        'application': 'binary',
        'learning_rate': 0.1,
        'min_data_in_leaf': 30,
        'num_leaves': 31,
        'max_depth': -1,
        'feature_fraction': 0.5,
        'scale_pos_weight': 2,
        'drop_rate': 0.02
    }
    logger.info("Training the model")
    cv_results = lgbm.cv(train_set=lgbm_train,
                         params=lgbm_params,
                         nfold=5,
                         num_boost_round=600,
                         early_stopping_rounds=50,
                         verbose_eval=50,
                         metrics=['auc'])

    optimum_boost_rounds = np.argmax(cv_results['auc-mean'])
    logger.info('Optimum boost rounds = {}'.format(optimum_boost_rounds))
    logger.info('Best CV result = {}'.format(np.max(cv_results['auc-mean'])))

    clf = lgbm.train(train_set=lgbm_train,
                     params=lgbm_params,
                     num_boost_round=optimum_boost_rounds)

    """" Save the model """
    directory = 'src/models/storage'
    if not os.path.exists(directory):
        os.makedirs(directory)
    model_id = "src/models/storage/lgbm_model_{}_{}.pkl".format(np.max(cv_results['auc-mean']),datetime.now().strftime('%Y%m%d_%H%M%S'))
    logger.info('Save model to {}'.format(model_id))
    joblib.dump(clf, model_id)

    return model_id


def LightGBM_prediction(model_id, test_df):
    if model_id is not None:
        clf = joblib.load(model_id)
    else:
        list_of_files = glob.glob('src/models/storage/*')
        latest_file = max(list_of_files, key=os.path.getctime)
        clf = joblib.load(latest_file)

    """ Predict on test set """
    y_pred = clf.predict(test_df)

    return y_pred
