import sys, os, gc
import pandas as pd

sys.path.append(os.path.join(os.path.dirname(__file__), "..", ".."))
os.chdir(os.path.join(os.path.dirname(__file__), "..", ".."))

from src.models.lgbm import LightGBM_prediction
from src.data.submission import get_submission_data, make_submission
from src.data.load import load_home_credit_risk_data
from src.features.features import process_dataframe, feature_engineering

def submit():
    '''
    Simply just make a prediction with the latest model and submit on Kaggle
    '''
    df_app_train, df_app_test, df_prev_app, df_bureau, df_bureau_balance, df_credit_card, df_pos_cash, df_install = load_home_credit_risk_data("data/home-credit-default-risk")
    df_test = pd.read_csv(os.path.join('data','home-credit-default-risk','application_test.csv.zip'))
    df_merged = feature_engineering(df_test, df_bureau, df_bureau_balance, df_credit_card,
                                    df_pos_cash, df_prev_app, df_install)

    # Separate metadata
    meta_cols = ['SK_ID_CURR', 'SK_ID_BUREAU']
    df_meta = df_merged[meta_cols]
    df_merged.drop(columns=meta_cols, inplace=True)

    del df_app_train, df_bureau, df_bureau_balance, df_credit_card, df_pos_cash, df_prev_app
    gc.collect()
    # Process the data set.
    df_labeled, categorical_feats, _ = process_dataframe(df_merged)

    y_pred = LightGBM_prediction(model_id = None, test_df = df_labeled)
    df_submission = get_submission_data(df_test, y_pred)
    response = make_submission(df_submission)

if __name__ == "__main__":
    submit()
