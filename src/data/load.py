import pandas as pd
import os
import sys
os.chdir(os.path.join(os.path.dirname(__file__), "..", ".."))
from src.utils.common_utils import check_kaggle_credential

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
logging.basicConfig(format='%(asctime)s | %(levelname)s : %(message)s',
                     level=logging.INFO, stream=sys.stdout)


def download_home_credit_risk_data(path):
    '''
        Download the competition data to the given path
    '''
    kaggle_api = check_kaggle_credential()
    logger.info("Downloading competition data started")
    kaggle_api.competitionDownloadFiles("home-credit-default-risk", path = path)

def load_home_credit_risk_data(input_dir):
    '''
        Import the csv data to Pandas

        :param input_dir: location of the csv files
        :type query: string

        :return: home credit risk data
        :rtype: list of DataFrames
    '''
    logger.info("Load data from csv files")
    df_app_train = pd.read_csv(os.path.join(input_dir,'application_train.csv.zip'))
    df_app_testtest = pd.read_csv(os.path.join(input_dir,'application_test.csv.zip'))
    df_prev_app = pd.read_csv(os.path.join(input_dir,'previous_application.csv.zip'))
    df_bureau= pd.read_csv(os.path.join(input_dir,'bureau.csv.zip'))
    df_bureau_balance = pd.read_csv(os.path.join(input_dir,'bureau_balance.csv.zip'))
    df_credit_card  = pd.read_csv(os.path.join(input_dir,'credit_card_balance.csv.zip'))
    df_pos_cash  = pd.read_csv(os.path.join(input_dir,'POS_CASH_balance.csv.zip'))
    df_install = pd.read_csv(os.path.join(input_dir,'installments_payments.csv.zip'))
    return(df_app_train,
        df_app_testtest,
        df_prev_app,
        df_bureau,
        df_bureau_balance,
        df_credit_card,
        df_pos_cash,
        df_install)
