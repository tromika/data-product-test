import pandas as pd
import sys, os
from datetime import datetime
import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
logging.basicConfig(format='%(asctime)s | %(levelname)s : %(message)s',
                     level=logging.INFO, stream=sys.stdout)

os.chdir(os.path.join(os.path.dirname(__file__), "..", ".."))

from src.utils.common_utils import check_kaggle_credential

def get_submission_data(df_test, y_pred):
    '''
        Combine the test data and Predictions

        :param df_test: original test data
        :type df_test: Pandas Dataframe
        :param y_pred: predictions
        :type y_pred: Numpy array

        :return: combined results
        :rtype: Pandas DataFrame
    '''
    logger.info("Combine the test data and predictions")
    return pd.DataFrame({'SK_ID_CURR': df_test['SK_ID_CURR'], 'TARGET': y_pred})

def make_submission(df_submission):
    '''
        Make a submission on kaggle
        Automatically uploads the data and make a submission

        :param df_submission: submission data
        :type df_submission: Pandas Dataframe

        :return: API response
        :rtype: String

    '''
    logger.info("Make submission")
    kaggle_api = check_kaggle_credential()
    #Persisting the data for kaggle api due it can only uploads files
    filename = 'data/submission_{}.csv.gz'.format(datetime.now().strftime('%Y%m%d_%H%M%S'))
    df_submission.to_csv(filename, index=False, compression='gzip')
    return kaggle_api.competitionSubmit(filename,
                                'test',
                                'home-credit-default-risk')
