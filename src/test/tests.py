import unittest
import os, gc, sys
import pandas as pd
from datetime import datetime

sys.path.append(os.path.join(os.path.dirname(__file__), "..", ".."))
os.chdir(os.path.join(os.path.dirname(__file__), "..", ".."))

from src.data.load import load_home_credit_risk_data, download_home_credit_risk_data
from src.features.features import process_dataframe, feature_engineering
from src.models.lgbm import LightGBM_model, LightGBM_prediction


files = ['application_train.csv.zip',
         'application_test.csv.zip',
         'previous_application.csv.zip',
         'bureau.csv.zip',
         'bureau_balance.csv.zip',
         'credit_card_balance.csv.zip',
         'POS_CASH_balance.csv.zip',
         'installments_payments.csv.zip']

data_folder = 'data/home-credit-default-risk'

class TestProject(unittest.TestCase):
    def test_project(self):
        '''
            Checking existence of downloaded files
        '''
        download_home_credit_risk_data("data/")
        for file in files:
            self.assertTrue(os.path.exists(os.path.join(data_folder,file)))

        df_app_train, df_app_test, df_prev_app, df_bureau, df_bureau_balance, df_credit_card, df_pos_cash, df_install = load_home_credit_risk_data("data/home-credit-default-risk")

        self.assertEqual(len(df_app_train), 307511)
        self.assertEqual(len(df_app_test), 48744)
        self.assertTrue(len(df_prev_app)>0)
        self.assertTrue(len(df_bureau)>0)
        self.assertTrue(len(df_bureau_balance)>0)
        self.assertTrue(len(df_pos_cash)>0)
        self.assertTrue(len(df_install)>0)

        len_train = len(df_app_train)
        df_app_both = pd.concat([df_app_train, df_app_test])
        df_merged = feature_engineering(df_app_both, df_bureau, df_bureau_balance, df_credit_card,
                                        df_pos_cash, df_prev_app, df_install)

        # Separate metadata
        meta_cols = ['SK_ID_CURR', 'SK_ID_BUREAU']
        df_meta = df_merged[meta_cols]
        df_merged.drop(columns=meta_cols, inplace=True)

        del df_app_train, df_bureau, df_bureau_balance, df_credit_card, df_pos_cash, df_prev_app
        gc.collect()
        # Process the data set.
        df_labeled, categorical_feats, _ = process_dataframe(df_merged)

        # Capture other categorical features not as object data types:
        non_obj_categoricals = [
            'FONDKAPREMONT_MODE', 'HOUR_APPR_PROCESS_START', 'HOUSETYPE_MODE',
            'NAME_EDUCATION_TYPE', 'NAME_FAMILY_STATUS', 'NAME_HOUSING_TYPE',
            'NAME_INCOME_TYPE', 'NAME_TYPE_SUITE', 'OCCUPATION_TYPE',
            'ORGANIZATION_TYPE', 'STATUS', 'NAME_CONTRACT_STATUS_CAVG',
            'WALLSMATERIAL_MODE', 'WEEKDAY_APPR_PROCESS_START', 'NAME_CONTRACT_TYPE_BAVG',
            'WEEKDAY_APPR_PROCESS_START_BAVG', 'NAME_CASH_LOAN_PURPOSE', 'NAME_CONTRACT_STATUS',
            'NAME_PAYMENT_TYPE', 'CODE_REJECT_REASON', 'NAME_TYPE_SUITE_BAVG',
            'NAME_CLIENT_TYPE', 'NAME_GOODS_CATEGORY', 'NAME_PORTFOLIO',
            'NAME_PRODUCT_TYPE', 'CHANNEL_TYPE', 'NAME_SELLER_INDUSTRY',
            'NAME_YIELD_GROUP', 'PRODUCT_COMBINATION', 'NAME_CONTRACT_STATUS_CCAVG'
        ]
        categorical_feats = categorical_feats + non_obj_categoricals

        # Re-separate into train and test
        df_train = df_labeled[:len_train]
        df_test = df_labeled[len_train:]
        del df_labeled
        gc.collect()

        self.assertEqual(df_train.shape[0], 307511)
        self.assertEqual(df_train.shape[1], 217)
        self.assertEqual(df_test.shape[0], 48744)
        self.assertEqual(df_test.shape[1], 217)

        model_id = LightGBM_model(df_train, df_test, categorical_feats)
        var_target = 'TARGET'
        train_columns = df_test.columns.drop([var_target]).tolist()
        y_pred = LightGBM_prediction(model_id,df_test[train_columns])

        self.assertEqual(y_pred, 48744)


if __name__ == '__main__':
    unittest.main()
