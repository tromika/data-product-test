import sys, os

sys.path.append(os.path.dirname(__file__))

from model.lgbm import LightGBM_prediction
from data.submission import get_submission_data, make_submission


def submit():
    '''
    Simply just make a prediction with the latest model and submit on Kaggle
    '''
    y_pred, auc = LightGBM_prediction(model_id = None):
    df_submission = get_submission_data(df_test, y_pred)
    response = make_submission(df_submission)

if __name__ == "__main__":
    submit()
