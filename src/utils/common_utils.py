import os, json
import shutil
import errno

def check_kaggle_credential():
    '''Check existence of kaggle credentials inside the container
       Fore more info pls check the Readme.md'''

    kaggle_cred = "/home/jovyan/.kaggle/kaggle.json"

    try:
        kaggle_username = os.environ["KAGGLE_USERNAME"]
        kaggle_key = os.environ["KAGGLE_KEY"]
    except:
        raise KeyError('Please set the kaggle_username and kaggle_key based on your kaggle.json')
    with open(kaggle_cred, 'w') as f:
        json.dump({"username":kaggle_username, "key":kaggle_key}, f)
    import kaggle.api as kaggle_api
    return kaggle_api
