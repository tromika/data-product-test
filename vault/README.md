# Vault usage

You can store you credential files in this folder to mount it to the container

## Kaggle credentials

You need a kaggle.json with an api token to be able to use Kaggle api to download files or submit your work.
You will find more instructions [here](https://colab.research.google.com/drive/1eufc8aNCdjHbrBhuy7M7X6BGyzAyRbrF#scrollTo=y5_288BYp6H1)
